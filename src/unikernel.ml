open V1_LWT
open Lwt
open Printf

module Main (C:CONSOLE) (S:STACKV4) = struct

    module T = S.TCPV4

    let udp_port = 18081
    let tcp_port = 18080

    let rec tcp_echo console flow =
        T.read flow
        >>= function
            | `Eof -> T.close flow
            | `Error _ -> T.close flow
            | `Ok b -> C.log_s console (sprintf " %d - %s" (Cstruct.len b) (Cstruct.to_string b))
                >>= fun () -> T.write flow b
                >>= function
                    | `Eof -> T.close flow
                    | `Error _ -> T.close flow
                    | `Ok () -> tcp_echo console flow

    let start console stack =
        S.listen_udpv4 stack udp_port (
            fun ~src ~dst ~src_port b ->
                C.log_s console (sprintf "udp: %s:%d" (Ipaddr.V4.to_string src) src_port)
                >>= fun () -> C.log_s console (sprintf " udp: %d - %s" (Cstruct.len b) (Cstruct.to_string b))
        );

        S.listen_tcpv4 stack tcp_port (
            fun flow ->
                let dst, dst_port = T.get_dest flow in
                C.log_s console (sprintf "tcp: %s:%d" (Ipaddr.V4.to_string dst) dst_port)
                >>= fun () -> tcp_echo console flow
        );

        S.listen stack
end
