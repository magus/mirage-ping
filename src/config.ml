open Mirage

let stack =
    let xenstack = direct_stackv4_with_dhcp default_console tap0 in
    (* let xenstack = direct_stackv4_with_default_ipv4 default_console tap0 in *)
    (* let xenstack = *)
    (*     let address = Ipaddr.V4.of_string_exn "192.168.0.200" in *)
    (*     let netmask = Ipaddr.V4.of_string_exn "255.255.255.0" in *)
    (*     let gateways = [Ipaddr.V4.of_string_exn "192.168.0.1"] in *)
    (*     direct_stackv4_with_static_ipv4 default_console tap0 { address; netmask; gateways } in *)
    match get_mode () with
    | `Xen -> xenstack
    | _ -> socket_stackv4 default_console [Ipaddr.V4.any]

let main = foreign "Unikernel.Main" (console @-> stackv4 @-> job)

let () = register "ping" [main $ default_console $ stack]
