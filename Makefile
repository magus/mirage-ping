MODE ?= unix

.PHONY : all configure build run clean

all : build
	@ :

configure :
	mirage configure src/config.ml --$(MODE)

build :
	cd src && make build

run :
	cd src && make run

clean :
	git clean -fdx
